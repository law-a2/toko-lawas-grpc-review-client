# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: review.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0creview.proto\x12\x06review\"\n\n\x08NoParams\"#\n\x11\x41llReviewResponse\x12\x0e\n\x06review\x18\x01 \x03(\t\"\x1b\n\tReviewDto\x12\x0e\n\x06review\x18\x01 \x01(\t2\x82\x01\n\rReviewService\x12;\n\x0cGetAllReview\x12\x10.review.NoParams\x1a\x19.review.AllReviewResponse\x12\x34\n\x0c\x43reateReview\x12\x11.review.ReviewDto\x1a\x11.review.ReviewDtob\x06proto3')



_NOPARAMS = DESCRIPTOR.message_types_by_name['NoParams']
_ALLREVIEWRESPONSE = DESCRIPTOR.message_types_by_name['AllReviewResponse']
_REVIEWDTO = DESCRIPTOR.message_types_by_name['ReviewDto']
NoParams = _reflection.GeneratedProtocolMessageType('NoParams', (_message.Message,), {
  'DESCRIPTOR' : _NOPARAMS,
  '__module__' : 'review_pb2'
  # @@protoc_insertion_point(class_scope:review.NoParams)
  })
_sym_db.RegisterMessage(NoParams)

AllReviewResponse = _reflection.GeneratedProtocolMessageType('AllReviewResponse', (_message.Message,), {
  'DESCRIPTOR' : _ALLREVIEWRESPONSE,
  '__module__' : 'review_pb2'
  # @@protoc_insertion_point(class_scope:review.AllReviewResponse)
  })
_sym_db.RegisterMessage(AllReviewResponse)

ReviewDto = _reflection.GeneratedProtocolMessageType('ReviewDto', (_message.Message,), {
  'DESCRIPTOR' : _REVIEWDTO,
  '__module__' : 'review_pb2'
  # @@protoc_insertion_point(class_scope:review.ReviewDto)
  })
_sym_db.RegisterMessage(ReviewDto)

_REVIEWSERVICE = DESCRIPTOR.services_by_name['ReviewService']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _NOPARAMS._serialized_start=24
  _NOPARAMS._serialized_end=34
  _ALLREVIEWRESPONSE._serialized_start=36
  _ALLREVIEWRESPONSE._serialized_end=71
  _REVIEWDTO._serialized_start=73
  _REVIEWDTO._serialized_end=100
  _REVIEWSERVICE._serialized_start=103
  _REVIEWSERVICE._serialized_end=233
# @@protoc_insertion_point(module_scope)
