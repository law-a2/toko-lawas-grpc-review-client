import grpc
import review_pb2 as pb
import review_pb2_grpc


class Client():

    # Create channel and stub to server's address and port.
    channel = grpc.insecure_channel('34.143.171.38.nip.io:50050')
    stub = review_pb2_grpc.ReviewServiceStub(channel)

    def get_all_review(self):
        # Exception handling.
        try:
            response = self.stub.GetAllReview(pb.NoParams())
            return response

        # Catch any raised errors by grpc.
        except grpc.RpcError as e:
            print("Error raised: " + e.details())

    def create_review(self, review):
        # Exception handling.
        try:
            response = self.stub.CreateReview(
                pb.ReviewDto(review=review))
            return response

        # Catch any raised errors by grpc.
        except grpc.RpcError as e:
            print("Error raised: " + e.details())
