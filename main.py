from http import client
import re
from client import Client
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, Request

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

grpc_client = Client()


@app.get("/reviews")
def read_root():
    all_review = grpc_client.get_all_review()
    reviews = []
    reviews.extend(all_review.review)
    return {"message": "success", "data": reviews}


@app.post("/reviews")
async def create_review(request: Request):
    req = await request.json()
    review = req["review"]
    res = grpc_client.create_review(review)
    return {"message": "success", "data": res.review}
